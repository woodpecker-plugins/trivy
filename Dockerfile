FROM ghcr.io/aquasecurity/trivy:0.60.0

RUN apk add --no-cache bash curl

ADD plugin.sh /bin/plugin.sh
ENTRYPOINT ["/bin/plugin.sh"]
