# Changelog

## [1.4.0](https://codeberg.org/woodpecker-plugins/trivy/releases/tag/v1.4.0) - 2025-02-16

### ❤️ Thanks to all contributors! ❤️

@pat-s, @woodpecker-bot

### ✨ Features

- Add private registry login option [[#27](https://codeberg.org/woodpecker-plugins/trivy/pulls/27)]

### 📚 Documentation

- Update registry default value [[#28](https://codeberg.org/woodpecker-plugins/trivy/pulls/28)]

### 📦️ Dependency

- chore(deps): update woodpeckerci/plugin-release docker tag to v0.2.4 [[#33](https://codeberg.org/woodpecker-plugins/trivy/pulls/33)]
- chore(deps): update woodpeckerci/plugin-release docker tag to v0.2.3 [[#30](https://codeberg.org/woodpecker-plugins/trivy/pulls/30)]
- chore(deps): update ghcr.io/aquasecurity/trivy docker tag to v0.58.2 [[#29](https://codeberg.org/woodpecker-plugins/trivy/pulls/29)]
- chore(deps): update woodpeckerci/plugin-docker-buildx docker tag to v5.2.0 [[#32](https://codeberg.org/woodpecker-plugins/trivy/pulls/32)]
- chore(deps): update docker.io/woodpeckerci/plugin-ready-release-go docker tag to v3.1.3 [[#31](https://codeberg.org/woodpecker-plugins/trivy/pulls/31)]
- chore(deps): update docker.io/woodpeckerci/plugin-ready-release-go docker tag to v3 [[#25](https://codeberg.org/woodpecker-plugins/trivy/pulls/25)]

## [1.3.0](https://codeberg.org/woodpecker-plugins/trivy/releases/tag/v1.3.0) - 2024-11-19

### ❤️ Thanks to all contributors! ❤️

@6543

### 📈 Enhancement

- Add timeout for waiting for server [[#23](https://codeberg.org/woodpecker-plugins/trivy/pulls/23)]

## [1.2.0](https://codeberg.org/woodpecker-plugins/trivy/releases/tag/v1.2.0) - 2024-11-02

### ❤️ Thanks to all contributors! ❤️

@6543, @woodpecker-bot

### ✨ Features

- Add service mode [[#19](https://codeberg.org/woodpecker-plugins/trivy/pulls/19)]
- Add option to change vulnerability database URL [[#17](https://codeberg.org/woodpecker-plugins/trivy/pulls/17)]

### 📈 Enhancement

- Only wait for server at the end of check execution [[#21](https://codeberg.org/woodpecker-plugins/trivy/pulls/21)]

### 📦️ Dependency

- chore(deps): update ghcr.io/aquasecurity/trivy docker tag to v0.57.0 [[#16](https://codeberg.org/woodpecker-plugins/trivy/pulls/16)]
- chore(deps): update woodpeckerci/plugin-docker-buildx docker tag to v5 [[#15](https://codeberg.org/woodpecker-plugins/trivy/pulls/15)]

### Misc

- Add tests [[#20](https://codeberg.org/woodpecker-plugins/trivy/pulls/20)]

## [1.1.1](https://codeberg.org/woodpecker-plugins/trivy/releases/tag/v1.1.1) - 2024-10-04

### ❤️ Thanks to all contributors! ❤️

@6543, @pat-s

### Misc

- chore(deps): update ghcr.io/aquasecurity/trivy docker tag to v0.56.1 [[#14](https://codeberg.org/woodpecker-plugins/trivy/pulls/14)]
- Use ready-release-go plugin [[#12](https://codeberg.org/woodpecker-plugins/trivy/pulls/12)]
