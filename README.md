# plugin-trivy

<p align="center">
  <a href="https://ci.codeberg.org/woodpecker-plugins/trivy" title="Build Status">
    <img src="https://ci.codeberg.org/api/badges/woodpecker-plugins/trivy/status.svg">
  </a>
  <a href="https://hub.docker.com/r/woodpeckerci/plugin-trivy" title="Docker pulls">
    <img src="https://img.shields.io/docker/pulls/woodpeckerci/plugin-trivy">
  </a>
  <a href="https://opensource.org/licenses/Apache-2.0" title="License: Apache-2.0">
    <img src="https://img.shields.io/badge/License-Apache%202.0-blue.svg">
  </a>
</p>

Woodpecker/Drone plugin to find vulnerabilities, misconfigurations, secrets, SBOM and more.
For the usage information and a listing of the available options please take a look at:

- <https://aquasecurity.github.io/trivy>
- [Docs](https://woodpecker-ci.org/plugins/Trivy)

## Build

Build the binary with the following commands:

```sh
docker buildx build -t woodpeckerci/plugin-trivy .
```

## Maintainers

This plugin is maintained by @6543.
