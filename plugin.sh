#!/bin/bash
set -e

ARGS_CHECK=()
ARGS_SERVICE=()

### SET DEFAULTS ###

# fail by default
if [ -z "${PLUGIN_EXIT_CODE}" ]; then
  PLUGIN_EXIT_CODE=1
fi

# ignore vendoring folders if var not defined
if [ "${PLUGIN_SKIP_DIRS+1}" != "1" ]; then
  PLUGIN_SKIP_DIRS="vendor,node_modules" # TODO: add more lang
fi

# rootdir to scan from
if [ -z "${PLUGIN_DIR}" ]; then
  PLUGIN_DIR="."
fi

# Set default server timeout to 1 minute if not specified
if [ -z "${PLUGIN_SERVER_TIMEOUT}" ]; then
  PLUGIN_SERVER_TIMEOUT=60
fi

### ARGS ###

if [ -n "${PLUGIN_SEVERITY}" ]; then
  ARGS_CHECK+=(--severity "${PLUGIN_SEVERITY}")
fi

if [ -n "${PLUGIN_REGISTRY_NAME}" ] && [ -n "${PLUGIN_REGISTRY_USERNAME}" ] && [ -n "${PLUGIN_REGISTRY_PASSWORD}" ]; then
  echo "Logging into registry '${PLUGIN_REGISTRY_NAME}' as user '${PLUGIN_REGISTRY_USERNAME}'"
  echo "${PLUGIN_REGISTRY_PASSWORD}" | trivy registry login --username ${PLUGIN_REGISTRY_USERNAME} --password-stdin ${PLUGIN_REGISTRY_NAME}
fi

if [ -n "${PLUGIN_DB_REPOSITORY}" ]; then
  ARGS_CHECK+=(--db-repository "${PLUGIN_DB_REPOSITORY}")
  ARGS_SERVICE+=(--db-repository "${PLUGIN_DB_REPOSITORY}")
fi

# Check for mutual exclusivity of PLUGIN_SERVER and PLUGIN_SERVICE
if [ -n "${PLUGIN_SERVER}" ] && [ -n "${PLUGIN_SERVICE}" ]; then
  echo "ERROR: Both PLUGIN_SERVER and PLUGIN_SERVICE are set. The plugin can not start in server and check mode at the sime time"
  exit 1
fi

### WAIT FOR SERVER IF SET ###
if [ -n "${PLUGIN_SERVER}" ]; then
  # Set default port if not specified
  if [ -n "${PLUGIN_SERVER_PORT}" ]; then
    SERVER_PORT="${PLUGIN_SERVER_PORT}"
  elif [ -n "${PLUGIN_SERVICE_PORT}" ]; then
    SERVER_PORT="${PLUGIN_SERVICE_PORT}"
  else
    SERVER_PORT_MSG="default "
    SERVER_PORT=10000
  fi

  # Check if PLUGIN_SERVER already contains a port number
  if [[ ! "${PLUGIN_SERVER}" =~ :[0-9]+$ ]]; then
    echo "INFO: No port detected in server URL, using ${SERVER_PORT_MSG}'${SERVER_PORT}'"
    PLUGIN_SERVER="${PLUGIN_SERVER}:${SERVER_PORT}"
  fi

  # fix incorrect server url schema
  if [[ ! "${PLUGIN_SERVER}" =~ ^https:// ]]; then
    # Check if there's a malformed schema
    if [[ "${PLUGIN_SERVER}" =~ ^[^:]+:// ]]; then
      # Extract the malformed schema for the warning message
      malformed_schema=$(echo "${PLUGIN_SERVER}" | sed -E 's/(:\/\/.*)$//')
      echo "WARNING: Malformed schema '${malformed_schema}://' detected, converting to 'http://'"
      # Remove malformed schema
      PLUGIN_SERVER=$(echo "${PLUGIN_SERVER}" | sed -E 's#^[^:]+://#/#')
      PLUGIN_SERVER=$(echo "${PLUGIN_SERVER}" | sed -E 's#^/+##')
      PLUGIN_SERVER="http://${PLUGIN_SERVER}"
    else
      # No schema found, add http://
      echo "INFO: No server schema found in server URL, adding 'http://'"
      PLUGIN_SERVER="http://${PLUGIN_SERVER}"
    fi
  fi

  ARGS_CHECK+=(--server "${PLUGIN_SERVER}")
fi

# Convert PLUGIN_SERVICE to uppercase for case-insensitive comparison
if [ -n "${PLUGIN_SERVICE}" ] && [ "$(echo "${PLUGIN_SERVICE}" | tr '[:lower:]' '[:upper:]')" = "TRUE" ]; then
  echo "INFO: Start in server mode"

  # Set default port if not specified
  if [ -n "${PLUGIN_SERVICE_PORT}" ]; then
    SERVICE_PORT="${PLUGIN_SERVICE_PORT}"
  elif [ -n "${PLUGIN_SERVER_PORT}" ]; then
    SERVICE_PORT="${PLUGIN_SERVER_PORT}"
  else
    SERVICE_PORT=10000
  fi

  if [ -n "${PLUGIN_DUMMY}" ]; then
    echo "DUMMY-EXEC: /usr/local/bin/trivy server --listen \"0.0.0.0:${SERVICE_PORT}\" ${ARGS_SERVICE[@]}"
  else
    echo /usr/local/bin/trivy server --listen "0.0.0.0:${SERVICE_PORT}" ${ARGS_SERVICE[@]}
    /usr/local/bin/trivy server --listen "0.0.0.0:${SERVICE_PORT}" ${ARGS_SERVICE[@]}
  fi

else
  echo "INFO: Start in check mode"

  if [ -n "${PLUGIN_DUMMY}" ]; then
    if [ -n "${PLUGIN_SERVER}" ]; then
      echo "INFO: Wait for server '${PLUGIN_SERVER}' to be available (timeout: ${PLUGIN_SERVER_TIMEOUT}s)"
    fi
    echo "DUMMY-EXEC: /usr/local/bin/trivy fs --exit-code ${PLUGIN_EXIT_CODE} --skip-dirs ${PLUGIN_SKIP_DIRS} ${ARGS_CHECK[@]} ${PLUGIN_DIR}"
  else

    if [ -n "${PLUGIN_SERVER}" ]; then
      echo "INFO: Wait for server '${PLUGIN_SERVER}' to be available (timeout: ${PLUGIN_SERVER_TIMEOUT}s)"

      start_time=$(date +%s)
      while [ $(curl "${PLUGIN_SERVER}" -s -o /dev/null || echo true) ]; do
        current_time=$(date +%s)
        elapsed_time=$((current_time - start_time))

        if [ ${elapsed_time} -ge ${PLUGIN_SERVER_TIMEOUT} ]; then
          echo "ERROR: Server connection timed out after ${PLUGIN_SERVER_TIMEOUT} seconds"
          exit 1
        fi

        sleep 1s
        echo -n .
      done
      echo " done"
    fi

    echo /usr/local/bin/trivy fs \
      --exit-code ${PLUGIN_EXIT_CODE} \
      --skip-dirs ${PLUGIN_SKIP_DIRS} \
      ${ARGS_CHECK[@]} ${PLUGIN_DIR}

    /usr/local/bin/trivy fs \
      --exit-code ${PLUGIN_EXIT_CODE} \
      --skip-dirs ${PLUGIN_SKIP_DIRS} \
      ${ARGS_CHECK[@]} ${PLUGIN_DIR}
  fi
fi
