#!/usr/bin/env bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Colors for output
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color

# Clear any existing PLUGIN_ environment variables
existing_vars=$(env | grep ^PLUGIN_ || true)
if [ -n "$existing_vars" ]; then
    echo -e "${YELLOW}WARNING: Found existing PLUGIN_ environment variables. Clearing them:${NC}"
    echo "$existing_vars"
    unset $(env | grep ^PLUGIN_ | cut -d= -f1)
fi

# Counter for tests
TESTS_TOTAL=0
TESTS_PASSED=0

# Helper function to run a test
run_test() {
    local test_name="$1"
    local expected="$2"
    shift 2

    ((TESTS_TOTAL++))

    echo "Running test: $test_name"

    # Run the command and capture output
    local output
    output=$(
        export PLUGIN_DUMMY=true
        while [ $# -gt 0 ]; do
            if [[ "$1" == *"="* ]]; then
                export "$1"
            fi
            shift
        done
        bash ${SCRIPT_DIR}/plugin.sh
    )

    local dummy_exec
    dummy_exec=$(echo "$output" | grep "$expected" || true)

    # Check if output matches expected string
    if [[ "$dummy_exec" == *"$expected"* ]]; then
        echo -e "${GREEN}✓ Test passed${NC}"
        ((TESTS_PASSED++))
    else
        echo -e "${RED}✗ Test failed${NC}"
        echo "Expected to find: $expected"
        echo "Got:"
        echo "$output"
    fi
    echo
}

# Test cases

# Test 1: Basic check mode
run_test "Basic check mode" \
"DUMMY-EXEC: /usr/local/bin/trivy fs --exit-code 1 --skip-dirs vendor,node_modules  ."

# Test 2: Server mode
run_test "Server mode" \
    "DUMMY-EXEC: /usr/local/bin/trivy server --listen \"0.0.0.0:10000\"" \
    PLUGIN_SERVICE=true

# Test 3: Custom port in server mode
run_test "Custom port server mode" \
    "DUMMY-EXEC: /usr/local/bin/trivy server --listen \"0.0.0.0:8080\"" \
    PLUGIN_SERVICE=true \
    PLUGIN_SERVICE_PORT=8080

# Test 4: Use server
run_test "Use server" \
    "DUMMY-EXEC: /usr/local/bin/trivy fs --exit-code 1 --skip-dirs vendor,node_modules --server http://localhost:10000 ." \
    PLUGIN_SERVER="localhost"

# Test 5: Malformed schema correction
run_test "Malformed schema correction" \
    "WARNING: Malformed schema 'htttps://' detected, converting to 'http://'" \
    PLUGIN_SERVER="htttps://localhost"

# Test 6: Check mutual exclusivity error
run_test "Mutual exclusivity check" \
    "ERROR: Both PLUGIN_SERVER and PLUGIN_SERVICE are set" \
    PLUGIN_SERVER="localhost" \
    PLUGIN_SERVICE=true

# Test 7: Custom severity
run_test "Custom severity" \
    "DUMMY-EXEC: /usr/local/bin/trivy fs --exit-code 1 --skip-dirs vendor,node_modules --severity HIGH" \
    PLUGIN_SEVERITY=HIGH

# Test 8: Server timeout message
run_test "Server timeout message" \
    "INFO: Wait for server 'http://localhost:10000' to be available (timeout: 30s)" \
    PLUGIN_SERVER="localhost" \
    PLUGIN_SERVER_TIMEOUT=30

# Print summary
echo "Test Summary:"
echo "Tests passed: $TESTS_PASSED/$TESTS_TOTAL"

# Exit with failure if any test failed
[ "$TESTS_PASSED" -eq "$TESTS_TOTAL" ] || exit 1
