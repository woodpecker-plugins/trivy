---
name: Trivy
author: Woodpecker Authors
icon: https://aquasecurity.github.io/trivy/v0.41/imgs/logo.png
description: Plugin to find vulnerabilities, misconfigurations, secrets, SBOM and more.
tags: [check, security, trivy, vulnerabilities, misconfigurations, secrets]
containerImage: woodpeckerci/plugin-trivy
containerImageUrl: https://hub.docker.com/r/woodpeckerci/plugin-trivy
url: https://codeberg.org/woodpecker-plugins/trivy
---

A plugin to find vulnerabilities, misconfigurations, secrets, SBOM and more.

The below pipeline configuration demonstrates simple usage:

```yml
pipeline:
  scan_vuln:
    image: woodpeckerci/plugin-trivy
```

## Settings

| Settings Name       | Default                           | Description                                                                                                  |
| ------------------- | --------------------------------- | ------------------------------------------------------------------------------------------------------------ |
| `exit-code`         | `1`                               | if an issue is detected let the step fail                                                                    |
| `skip-dirs`         | `vendor,node_modules`             | folders excluded from scan                                                                                   |
| `dir`               | `.`                               | root folder to scan from                                                                                     |
| `server`            | _none_                            | use a trivy server, can be a service step or extern                                                          |
| `severity`          | _none_                            | severities of security issues to be displayed (comma separated) (default "UNKNOWN,LOW,MEDIUM,HIGH,CRITICAL") |
| `db-repository`     | `ghcr.io/aquasecurity/trivy-db:2` | specify the OCI registry URL to retrieve the vulnerability database. (e.g. `docker.io/aquasec/trivy-db:2`)   |
| `registry-name`     | _none_                            | Private registry name                                                                                        |
| `registry-username` | _none_                            | Private registry username                                                                                    |
| `registry-password` | _none_                            | Private registry password                                                                                    |

## Advanced settings

| Settings Name                  | Default | Description                                                          |
| ------------------------------ | ------- | -------------------------------------------------------------------- |
| `service`                      | `false` | start trivy in server mode, instead of scanning                      |
| `service-port` / `server-port` | `10000` | the port trivy server will listen to                                 |
| `server-timeout`               | `60`    | timeout the scanner waits for an server to be reachable (in seconds) |
